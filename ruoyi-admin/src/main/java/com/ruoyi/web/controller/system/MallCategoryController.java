package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MallCategory;
import com.ruoyi.system.service.IMallCategoryService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 分类Controller
 * 
 * @author ruoyi
 * @date 2021-07-04
 */
@Controller
@RequestMapping("/system/category")
public class MallCategoryController extends BaseController
{
    private String prefix = "system/category";

    @Autowired
    private IMallCategoryService mallCategoryService;

    @RequiresPermissions("system:category:view")
    @GetMapping()
    public String category()
    {
        return prefix + "/category";
    }

    /**
     * 查询分类树列表
     */
    @RequiresPermissions("system:category:list")
    @PostMapping("/list")
    @ResponseBody
    public List<MallCategory> list(MallCategory mallCategory)
    {
        List<MallCategory> list = mallCategoryService.selectMallCategoryList(mallCategory);
        return list;
    }

    /**
     * 导出分类列表
     */
    @RequiresPermissions("system:category:export")
    @Log(title = "分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MallCategory mallCategory)
    {
        List<MallCategory> list = mallCategoryService.selectMallCategoryList(mallCategory);
        ExcelUtil<MallCategory> util = new ExcelUtil<MallCategory>(MallCategory.class);
        return util.exportExcel(list, "分类数据");
    }

    /**
     * 新增分类
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("mallCategory", mallCategoryService.selectMallCategoryById(id));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存分类
     */
    @RequiresPermissions("system:category:add")
    @Log(title = "分类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MallCategory mallCategory)
    {
        return toAjax(mallCategoryService.insertMallCategory(mallCategory));
    }

    /**
     * 修改分类
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        MallCategory mallCategory = mallCategoryService.selectMallCategoryById(id);
        mmap.put("mallCategory", mallCategory);
        return prefix + "/edit";
    }

    /**
     * 修改保存分类
     */
    @RequiresPermissions("system:category:edit")
    @Log(title = "分类", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MallCategory mallCategory)
    {
        return toAjax(mallCategoryService.updateMallCategory(mallCategory));
    }

    /**
     * 删除
     */
    @RequiresPermissions("system:category:remove")
    @Log(title = "分类", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id)
    {
        return toAjax(mallCategoryService.deleteMallCategoryById(id));
    }

    /**
     * 选择分类树
     */
    @GetMapping(value = { "/selectCategoryTree/{id}", "/selectCategoryTree/" })
    public String selectCategoryTree(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("mallCategory", mallCategoryService.selectMallCategoryById(id));
        }
        return prefix + "/tree";
    }

    /**
     * 加载分类树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = mallCategoryService.selectMallCategoryTree();
        return ztrees;
    }
}
