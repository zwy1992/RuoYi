package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.ArrayList;
import com.ruoyi.common.core.domain.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MallCategoryMapper;
import com.ruoyi.system.domain.MallCategory;
import com.ruoyi.system.service.IMallCategoryService;
import com.ruoyi.common.core.text.Convert;

/**
 * 分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-04
 */
@Service
public class MallCategoryServiceImpl implements IMallCategoryService 
{
    @Autowired
    private MallCategoryMapper mallCategoryMapper;

    /**
     * 查询分类
     * 
     * @param id 分类ID
     * @return 分类
     */
    @Override
    public MallCategory selectMallCategoryById(Long id)
    {
        return mallCategoryMapper.selectMallCategoryById(id);
    }

    /**
     * 查询分类列表
     * 
     * @param mallCategory 分类
     * @return 分类
     */
    @Override
    public List<MallCategory> selectMallCategoryList(MallCategory mallCategory)
    {
        return mallCategoryMapper.selectMallCategoryList(mallCategory);
    }

    /**
     * 新增分类
     * 
     * @param mallCategory 分类
     * @return 结果
     */
    @Override
    public int insertMallCategory(MallCategory mallCategory)
    {
        return mallCategoryMapper.insertMallCategory(mallCategory);
    }

    /**
     * 修改分类
     * 
     * @param mallCategory 分类
     * @return 结果
     */
    @Override
    public int updateMallCategory(MallCategory mallCategory)
    {
        return mallCategoryMapper.updateMallCategory(mallCategory);
    }

    /**
     * 删除分类对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMallCategoryByIds(String ids)
    {
        return mallCategoryMapper.deleteMallCategoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除分类信息
     * 
     * @param id 分类ID
     * @return 结果
     */
    @Override
    public int deleteMallCategoryById(Long id)
    {
        return mallCategoryMapper.deleteMallCategoryById(id);
    }

    /**
     * 查询分类树列表
     * 
     * @return 所有分类信息
     */
    @Override
    public List<Ztree> selectMallCategoryTree()
    {
        List<MallCategory> mallCategoryList = mallCategoryMapper.selectMallCategoryList(new MallCategory());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (MallCategory mallCategory : mallCategoryList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(mallCategory.getId());
            ztree.setpId(mallCategory.getParentId());
            ztree.setName(mallCategory.getName());
            ztree.setTitle(mallCategory.getName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
