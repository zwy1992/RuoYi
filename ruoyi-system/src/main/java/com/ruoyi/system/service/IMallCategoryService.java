package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MallCategory;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 分类Service接口
 * 
 * @author ruoyi
 * @date 2021-07-04
 */
public interface IMallCategoryService 
{
    /**
     * 查询分类
     * 
     * @param id 分类ID
     * @return 分类
     */
    public MallCategory selectMallCategoryById(Long id);

    /**
     * 查询分类列表
     * 
     * @param mallCategory 分类
     * @return 分类集合
     */
    public List<MallCategory> selectMallCategoryList(MallCategory mallCategory);

    /**
     * 新增分类
     * 
     * @param mallCategory 分类
     * @return 结果
     */
    public int insertMallCategory(MallCategory mallCategory);

    /**
     * 修改分类
     * 
     * @param mallCategory 分类
     * @return 结果
     */
    public int updateMallCategory(MallCategory mallCategory);

    /**
     * 批量删除分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMallCategoryByIds(String ids);

    /**
     * 删除分类信息
     * 
     * @param id 分类ID
     * @return 结果
     */
    public int deleteMallCategoryById(Long id);

    /**
     * 查询分类树列表
     * 
     * @return 所有分类信息
     */
    public List<Ztree> selectMallCategoryTree();
}
